require('dotenv').config()
const config = require('config')
const Agenda = require('agenda')
const axios = require('axios')
const async = require('async')
const q = require('q')
const moment = require('moment')
const _ = require('lodash')
const uuid = require('uuid-by-string')
const r = require('rethinkdbdash')({
  servers: [config.rethinkdb]
})
const agenda = new Agenda({ db: { address: config.mongodb } })
let count = 0
const factors = {
  BTC: 8,
  ETH: 18,
  LTC: 8
}
const tokens = {}
const synchronize = async () => {
  console.log('---------- Starting Synchronization ----------')
  count = 0
  await prepareDB()
  const transactions = await retrieveInputs()
  await populateTokens()
  async.eachLimit(transactions, 10, async tx => {
    await process(tx).catch(err => {
      console.error(`[ERROR] ${err.response ? JSON.stringify(err.response.data.error) : err}`)
    })
    count += 1
    const percent = (Math.round(count * 1000.0 / transactions.length) / 10).toFixed(1)
    console.error(`[${percent}%] Processed ${count} of ${transactions.length}`)
  }, err => {
    if (err) console.error(`[ERROR] ${err}`)
    console.log('---------- Synchronization Complete ----------')
  })
}
const prepareDB = async () => {
  const dbs = await r.dbList().run()
  if (dbs.indexOf(config.rethinkdb.db) === -1) {
    await r.dbCreate(config.rethinkdb.db)
  }
  const tables = await r.db(config.rethinkdb.db).tableList().run()
  if (tables.indexOf('transactions') === -1) {
    await r.db(config.rethinkdb.db).tableCreate('transactions').run()
  }
}
const retrieveInputs = async () => {
  const transactions = []
  console.log('Retrieving Brickblock inputs...')
  await axios.get(config.inputs).then(d => d.data).then(d => {
    d.preIco.map(tx => { transactions.push({ ...tx, type: 'pre' }) })
    d.ico.map(tx => { transactions.push({ ...tx, type: 'ico' }) })
  })
  const esids = await r.db(config.rethinkdb.db).table('transactions').getField('hash').run()
  const newtxs = transactions.filter(tx => esids.indexOf(tx.txid) === -1)
  console.log(`Found ${newtxs.length} transactions`)
  return newtxs
}
const populateTokens = async () => {
  const def = q.defer()
  async.each(config.api.tokens, async token => {
    const url = `${config.api.url}/tokens/${token}`
    const data = await axios.get(url).then(d => d.data)
    const hour = data.limits['api/hour'] - (data.hits && data.hits['api/hour'] ? data.hits['api/hour'] : 0)
    const second = data.limits['api/second'] - (data.hits && data.hits['api/second'] ? data.hits['api/second'] : 0)
    const remaining = { hour, second }
    tokens[token] = remaining
  }, err => {
    if (err) def.reject(err)
    else def.resolve()
  })
  return def.promise
}
const getToken = async () => {
  const def = q.defer()
  const token = _.sample(_.keys(tokens), t => tokens[t].hour > 0 && tokens[t].second > 0)
  if (!token) {
    setTimeout(async () => {
      console.log('Waiting for token...')
      const t = await getToken()
      def.resolve(t)
    }, 200)
  }
  else def.resolve(token)
  return def.promise
}
const convLimitWait = async () => {
  const url = `${config.convert}/stats/rate/limit`
  const limits = await axios.get(url).then(d => d.data)
  const hit = limits.Hour.CallsLeft.Histo <= 0 || limits.Minute.CallsLeft.Histo <= 0 || limits.Second.CallsLeft.Histo <= 0
  if (hit) await convLimitWait()
}
const process = async (tx) => {
  const token = await getToken()
  const url = `${config.api.url}/${tx.currency.toLowerCase()}/main/txs/${tx.txid}?token=${token}`
  const resp = await axios.get(url).catch(async err => {
    if (err.response && err.response.status === 429) {
      console.error(`Hit the rate limit for ${token}. Remaining: ${JSON.stringify(tokens[token])}`)
    }
    else {
      console.error(`[ERROR] ${err.response ? JSON.stringify(err.response.data.error) : err}`)
    }
  })
  if (resp) {
    const data = { ...resp.data, type: tx.type, currency: tx.currency }
    const factor = Math.pow(10, factors[data.currency])
    data.total = data.total / factor
    data.fees = data.fees / factor
    data.gas_price = data.gas_price / factor
    data.outputs.map(op => { op.value = op.value / factor })
    let conversion = 1
    if (data.currency !== 'ETH') {
      const convURL = `${config.convert}/data/pricehistorical?fsym=${data.currency}&tsyms=ETH&ts=${moment(data.received).unix()}`
      await convLimitWait()
      conversion = await axios.get(convURL).then(d => d.data[data.currency] ? d.data[data.currency].ETH : 0)
    }
    const converted = {
      total: data.total * conversion,
      fees: data.fees * conversion
    }
    data.converted = converted
    if (!data.gas_price) data.gas_price = 0
    data.id = uuid(`${data.hash}-${data.addresses.join('-')}`)
    await r.db(config.rethinkdb.db).table('transactions').insert(data)
  }
}
agenda.define('sync', (job, done) => {
  synchronize().then(done).catch(done)
})

agenda.on('ready', () => {
  agenda.every('5 minutes', 'sync')
  if (config.env === 'development') {
    synchronize()
  }
  else {
    agenda.start()
  }
  console.log('---------- Started sync server ----------')
})