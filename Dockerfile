FROM node:latest
COPY . /app
WORKDIR /app
RUN npm install --only production
ENTRYPOINT [ "npm", "start" ]